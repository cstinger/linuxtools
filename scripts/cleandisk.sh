#!/bin/bash

_UPD="N"
_ShDiskUse="Y"
_Clean="Y"

echo -n "****** Do you want to update/upgrade the system? ******(y/N):"
read _UPD
echo $_UPD
if [ "${_UPD}" == "y" -o "${_UPD}" == "Y" ]; then
	sudo apt update && sudo apt dist-upgrade
fi

echo -n "****** Show disk usage before clean? ******(Y/n):"
read _ShDiskUse
echo $_ShDiskUse
if [ ! "$_ShDiskUse" == "N" -a ! "$_ShDiskUse" == "n" ]; then
	sudo du -sh /var/cache/apt
	journalctl --disk-usage
	du -h /var/lib/snapd/snaps
	du -h ~/.cache/thumbnails
fi

echo -n "****** Clean the disk? ******(Y/n):"
read _Clean
echo $_Clean
if [ ! "$_Clean" == "N" -a ! "$_Clean" == "n" ]; then

	sudo apt autoremove
	sudo apt clean
	sudo journalctl --vacuum-time=3d
	echo "****** Now removes old revisions of snaps ******"
	echo "****** CLOSE ALL SNAPS BEFORE RUNNING THIS ******"
	_SNAPC=`ps ax | grep -v snapd | grep -v grep | grep -v snap-store | grep -c "/snap/"`
	if [ $_SNAPC -eq "0" ]; then
		set -eu
		snap list --all | awk '/disabled/{print $1, $3}' |
			while read snapname revision; do
				sudo snap remove "$snapname" --revision="$revision"
			done
	else
		echo "****** Running SNAPS found, stop for cleaning SNAPS ******"
	fi

	rm -rf ~/.cache/thumbnails/*
fi

echo "****** Done! please check if any problems there ******"
