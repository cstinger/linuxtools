#!/bin/sh
# wifictrl.sh
# Ref: https://www.digitalairwireless.com/articles/blog/wifi-transmit-power-calculations-made-simples
#  0dBm = 1mW
#  3dBm = 2mW
#  6dBm = 4mW
#  9dBm = 8mW
# 12dBm = 16mW
# 15dBm = 32mW
# 18dBm = 64mW
# 21dBm = 128mW
# 24dBm = 256mW
# 27dBm = 512mW
# 30dBm = 1024mW (5GHz Band B legal limit)
# 33dBm = 2048mW
# 36dBm = 4096mW (5GHz Band C legal limit)

_chk5gc=0

_wifi5ssid="A0_5G"

if [ ! -n "$1" ]; then
	echo "*** usage: $0 <parameters> <options> ***
*****************************************************************************
  <parameters> = 
  chk5g: check if 5G works correctly, or restart it till works or 25 trials
  5: set the txpower of 5G radio to <options>=0~30 dBm
  2: set the txpower of 2G radio to <options>=0~30 dBm
  all: set the txpower of both radios to <options> dBm
  auto: check current time and adjust the radio txpower to suitable values
  powersaving: set the radio txpower to usable, but lower power consumption
  max: set the radio txpower to 20dBm (100mW, default maximum)
  getpwr: get all the txpower value of all bands
  <options>=over: set the radio txpower to the limitation (30dBm or 1000mW)
  <options>=get/getcfg: get the txpower value of specified radio band
*****************************************************************************"

elif [ "$1" == "chk5g" ]; then
	while [ ! -n "`iwinfo | grep '$_wifi5ssid'`" -a $_chk5gc -lt 25 ]; do
		/bin/sleep 5
		/sbin/wifi up radio0
		echo $_chk5gc >> /dev/shm/5gc.log
		_chk5gc=`expr $_chk5gc + 1`
	done
	if [ -n "`iwinfo | grep '$_wifi5ssid'`" ]; then
		echo "*** 5G wifi (radio0) enabled successfully ***"
	elif [ $_chk5gc -ge 24 ]; then
		echo "*** reach the limit of retrial ***"
	else
		echo "*** Something go wrong, suggested to have a check manually ***"
	fi
elif [ "$1" == "all" ]; then
	if [ $2 -gt 0 -a $2 -le 30 ]; then
		$0 5 $2
		sleep 1
		$0 2 $2
	else
		$0
	fi
elif [ "$1" == "auto" ]; then
	if [ `date +%H` -le 06 -o `date +%H` -ge 23 ]; then 
		/sbin/wifi down radio0	# shutdown 5G
		$0 2 20
	else 
		$0 5 20
		$0 2 20
	fi
elif [ "$1" == "powersaving" ]; then
	$0 5 12		# 16mW
	$0 2 18		# 64mW
elif [ "$1" == "max" ]; then
	$0 5 20
	$0 2 20
elif [ "$1" == "getpwr" ]; then
	$0 5 get
	$0 2 get
elif [ "$1" == "getcfg" ]; then
	$0 5 getcfg
	$0 2 getcfg
elif [ $1 -eq 5 -o $1 -eq 2 ]; then
        _rn=`expr -$1 / 3 + 5 / 3`      # 5 -> 0; 2 -> 1
        if [ "$2" == "over" ]; then             # suggestion: only for 5G
                if [ $1 -eq 5 ]; then
                        $0 5 23
                else
                        $0 2 30
                fi
        elif [ "$2" == "getcfg" ]; then
		echo -n "radio$_rn (wifi$1G):	"
                /sbin/uci get wireless.radio${_rn}.txpower
        elif [ "$2" == "get" ]; then
                iwinfo wlan$_rn info | head -1
                iwinfo wlan$_rn info | grep Power
                iwinfo wlan$_rn-1 info | head -1
                iwinfo wlan$_rn-1 info | grep Power
        elif [ $2 -gt 0 -a $2 -le 30 ]; then
                /sbin/wifi down radio${_rn}
                /sbin/uci set wireless.radio${_rn}.txpower=$2 && /sbin/uci commit && /sbin/wifi up radio${_rn}
                /bin/sleep 1
        elif [ $2 -eq 0 ]; then
                /sbin/wifi down radio${_rn}
        else
                $0
        fi
else
	$0
fi	
